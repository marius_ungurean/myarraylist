import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MyArrayListTest {

    MyArrayList<Integer> intList;

    @BeforeEach
    void setUp() {
        intList = new MyArrayList<>();
        intList.add(1);
        intList.add(2);
    }

    @Test
    void add() {
        intList.add(10);
        assertEquals(3, intList.size());
    }

    @Test
    void addAtIndex() {
        intList.add(10, 0);
        assertEquals(10, intList.get(0));
    }

    @Test
    void remove() {
        intList.add(10);
         int value = intList.remove(1);
        assertEquals(2, value);
        assertEquals(2, intList.size());
        assertEquals(10, intList.get(1));
    }

    @Test
    void get() {
        assertEquals(1, intList.get(0));
    }

    @Test
    void indexOf() {
        assertEquals(0, intList.indexOf(1));
    }

    @Test
    void size() {
        assertEquals(2, intList.size());
    }
}