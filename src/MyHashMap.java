import java.io.Serializable;

public class MyHashMap<K, V> implements Cloneable, Serializable {

    public int size() {
        return size;
    }

    private static class Node<K, V> {
        private final K key;
        private V value;
        private Node<K, V> next;

        public Node(K key, V value, Node<K, V> next) {
            this.key = key;
            this.value = value;
            this.next = next;
        }
    }

    private Node<K, V>[] buckets;
    private static final int INITIAL_CAPACITY = 1 << 4;
    int size = 0;

    public MyHashMap() {
        this(INITIAL_CAPACITY);
    }

    @SuppressWarnings("unchecked")
    public MyHashMap(int capacity) {
        buckets = new Node[capacity];
    }

    public void put(K key, V value) {
        Node<K, V> node = new Node<>(key, value, null);
        int index;
        if (key == null) index = 0;
        else
            index = hashCodeNew(key) % INITIAL_CAPACITY - 1;
        Node<K, V> current = buckets[index];
        if (current == null) {
            buckets[index] = node;
            size++;
        } else {
            while (current.next != null) {
                if (current.key.equals(key)) {
                    current.value = value;
                    return;
                }
                current = current.next;
            }
            if (current.key.equals(key)) {
                current.value = value;
            } else {
                current.next = node;
                size++;
            }
        }
    }

    public V get(K key) {
        Node<K, V> node = buckets[hashCodeNew(key) % INITIAL_CAPACITY - 1];
        while (node != null) {
            if (node.key.equals(key))
                return node.value;
            node = node.next;
        }
        return null;
    }


    private int hashCodeNew(K key) {
        return (int) key.toString().charAt(0);
    }
}