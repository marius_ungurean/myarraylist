import java.util.Arrays;

public class MyArrayList<T> {

    private T[] elements;
    private int lastIndex = 0;

    @SuppressWarnings("unchecked")
    public MyArrayList() {
        elements = (T[]) (new Object[10]);
    }

    @SuppressWarnings("unchecked")
    public MyArrayList(int size) {
        elements = (T[]) (new Object[size]);
    }

    public boolean add(T element) {
        if (maxCapacity()) doubleCapacity();
        elements[lastIndex] = element;
        lastIndex++;
        return true;
    }

    public void add(T element, int index) {
        if (checkIndex(index)) {
            elements[index] = element;
        }
    }

    public T remove(int index) {
        T elem = null;
        if (checkIndex(index)) {
            elem = elements[index];
            for (int i = index; i < lastIndex - 1; i++) {
                elements[index] = elements[index + 1];
            }
            elements[lastIndex] = null;
            lastIndex--;
        }
        return elem;
    }

    public T get(int index) {
        return checkIndex(index) ? elements[index] : null;
    }

    public int indexOf(T value) {
        for (int i = 0; i < lastIndex; i++) {
            if (elements[i].equals(value)) return i;
        }
        return -1;
    }

    private void doubleCapacity() {
        elements = Arrays.copyOf(elements, lastIndex * 2);
    }

    private boolean maxCapacity() {
        return lastIndex == elements.length - 1;
    }

    private boolean checkIndex(int index) {
        if (index < 0 || index > lastIndex) throw new IndexOutOfBoundsException();
        return true;
    }

    public int size() {
        return lastIndex;
    }


}
